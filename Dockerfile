FROM ubuntu:16.04

WORKDIR /app

COPY /build .
COPY nginx.conf /etc/nginx/sites-enabled/

RUN apt-get update && \
    apt-get install -yq nginx && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 8080

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

CMD nginx -g "daemon off;" 